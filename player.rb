# encoding: utf-8
class Player
  attr_reader :color

  def initialize(color)
    @color = color
  end

  def play_turn(board)
    puts board.render
    puts "Current Player is #{color}: "

    from_pos = prompt("Select the piece you want to move (format: 2,2): ")
    piece = board[from_pos]

    move_sequence = []
    to_pos = prompt("Input move to position (format: 1,1): ")
    move_sequence << to_pos
    p move_sequence

    puts "Add another position? y/n: "
    more_pos = gets.chomp

    if more_pos == 'y'
      to_pos = prompt("Input move to position (format: 1,1): ")
      move_sequence << to_pos
    end

    p move_sequence
    piece.perform_moves(move_sequence)
  end

  def prompt(msg)
    puts msg
    pos = gets.chomp.split(",")
    row = Integer(pos[0])
    col = Integer(pos[1])

    [row, col]
  end
end
